<html>
	<head>
		<link rel="stylesheet" href="/ZE/style.css"> 
		<meta charset="UTF-8">
	</head>
	<body>
  <?php error_reporting(E_ALL);
    session_start();
    // Zum Aufbau der Verbindung zur Datenbank
    define ( 'MYSQL_HOST',      'localhost' );
    define ( 'MYSQL_BENUTZER',  'root' );
    define ( 'MYSQL_KENNWORT',  '' );
    define ( 'MYSQL_DATENBANK', 'timerec' );

    $db_link = mysqli_connect (MYSQL_HOST, MYSQL_BENUTZER, MYSQL_KENNWORT, MYSQL_DATENBANK);
  ?>

	<nav> 
	<tl>TIMETABLE</tl>
	</nav> 

	<main>
	<?php 
	$sql = "SELECT * FROM `tbl_user`";
	$users = $db_link->query($sql);
	
  if ($users->num_rows > 0) {
    while($row = $users->fetch_assoc()) {
      echo "id: " . $row["user_id"]. " - Name: " . $row["fname"]. " " . $row["lname"]. "<br>";
    }
  } else {
    echo "0 results";
  }
	?>

<form action="action_page.php" method="post">
  <div class="main">
    <div class="container">
      <label for="uname"><b>Benutzername</b></label>
      <input type="text" placeholder="Benutzername" name="uname" required>

      <label for="psw"><b>Passwort</b></label>
      <input type="password" placeholder="Passwort" name="psw" required>

      <button type="submit">Login</button>
      <!--<label>
        <input type="checkbox" checked="checked" name="remember"> Remember me
      </label>-->
    <span class="psw"> <a href="#">Passwort vergessen?</a></span>
    </div>
      <!--<div class="container" style="background-color:#f1f1f1">-->
      <!--<button type="button" class="cancelbtn">Cancel</button>-->
      
      <!--</div>-->
    <div class="imgcontainer">
      <img src="avatar.png" alt="Avatar" class="avatar">
    </div>
  </div>
</form> 

</main>
	</body>
</html>