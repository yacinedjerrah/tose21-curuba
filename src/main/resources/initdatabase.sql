create table if not exists tbl_benutzer (
id int identity(1,1) primary key,
name nvarchar(255) unique NOT NULL CHECK (name <> ''),
password nvarchar(255) NOT NULL CHECK (password <> ''),
pensum int,
ferien int,
admin boolean,
ueberzeit int
);

create table if not exists tbl_kostenstelle (
id int identity(1,1) primary key,
name nvarchar(255) unique NOT NULL CHECK (name <> ''),
nr int unique NOT NULL CHECK (nr <> null),
ort varchar(255) CHECK (ort <> '')
);

create table if not exists tbl_ferien (
id int identity(1,1) primary key,
arbeiterid int,
ferientage decimal NOT NULL CHECK(ferientage <> null),
von date NOT NULL,
bis date NOT NULL,
genehmigt boolean,
FOREIGN KEY (arbeiterid) REFERENCES tbl_benutzer(id)
);

create table if not exists tbl_arbeitszeit (
id int identity(1,1) primary key,
arbeiterid int,
arbeitszeit decimal NOT NULL,
eintrag varchar(255) NOT NULL,
datum date NOT NULL,
FOREIGN KEY (arbeiterid) REFERENCES tbl_benutzer(id),
FOREIGN KEY (eintrag) REFERENCES tbl_kostenstelle(name)
);

create table if not exists tbl_session (
id int identity(1,1) primary key,
name varchar(255),
FOREIGN KEY (name) REFERENCES tbl_benutzer(name)
);

create table if not exists tbl_sollzeit (
id int identity(1,1) primary key,
jahr int NOT NULL CHECK (jahr <> 0),
monat int NOT NULL CHECK (monat <> 0),
arbeitstage int NOT NULL,
zeit int NOT NULL
); 

create table if not exists tbl_betriebsferien (
id int identity(1,1) primary key,
von date NOT NULL,
bis date NOT NULL,
eintrag varchar(255) NOT NULL
);

MERGE INTO TBL_BENUTZER KEY(ID) VALUES(1, 'testadmin' , 'test', 100, 100, 1 , 0);
MERGE INTO TBL_BENUTZER KEY(ID) VALUES(2, 'test' , 'tester', 100, 100, 0 , 0);
MERGE INTO TBL_SOLLZEIT KEY(ID) VALUES(1, 2021, 6, 22, 187)