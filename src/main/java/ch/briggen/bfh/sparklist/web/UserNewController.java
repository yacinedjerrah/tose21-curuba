package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class UserNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UserNewController.class);
		
	
	private UserRepository userRepo = new UserRepository();
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User userDetail = UserWebHelper.userFromWeb(request);
		HashMap<String, Object> model = new HashMap<String, Object>();
		log.trace("POST /benutzer/anlegen mit userDetail 123123" + userDetail);
		long id = userRepo.insert(userDetail);
		
		userRepo.insert(userDetail);
		
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (id == 0) {
			model.put("isadmin", "true");
			model.put("postAction", "/benutzer/anlegen");
			model.put("pfad", "usernew");
			model.put("userDetail", new User());
			model.put("falsch", true);
			return new ModelAndView(model, "userDetailTemplate");
		} else {
			response.redirect("/benutzer");
			return null;
			}
		
	}
}


