package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Betriebsferien;
import ch.briggen.bfh.sparklist.domain.BetriebsferienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BetriebsferienUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(BetriebsferienUpdateController.class);
		
	private BetriebsferienRepository betriebsferienRepo = new BetriebsferienRepository();
	


	/**
	 * Schreibt das geänderte Item zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /item/update
	 * 
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Betriebsferien betriebsferienDetail = BetriebsferienWebHelper.betriebsferienFromWeb(request);
		
		log.trace("POST /betriebsferien/aktualisieren mit ferienDetail " + betriebsferienDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		betriebsferienRepo.save(betriebsferienDetail);
		//response.redirect("/ferien?id="+ferienDetail.getId());
		response.redirect("/betriebsferien");

		return null;
	}
}


