package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kostenstelle;
import spark.Request;

class KostenstelleWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(KostenstelleWebHelper.class);
	
	public static Kostenstelle KostenstelleFromWeb(Request request)
	{
		return new Kostenstelle(
				Integer.parseInt(request.queryParams("customerDetail.id")),
				request.queryParams("customerDetail.name"),
				Integer.parseInt(request.queryParams("customerDetail.nr")),
				request.queryParams("customerDetail.ort"));
	}

}
