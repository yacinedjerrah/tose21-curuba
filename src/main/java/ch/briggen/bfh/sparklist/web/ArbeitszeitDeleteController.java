package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ArbeitszeitRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ArbeitszeitDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ArbeitszeitDeleteController.class);
		
	private ArbeitszeitRepository arbeitszeitRepo = new ArbeitszeitRepository();
	

	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /arbeitszeit/delete mit id " + id);
		
		int intId = Integer.parseInt(id);
		arbeitszeitRepo.delete(intId);
		response.redirect("/arbeitszeit");
		return null;
	}
}


