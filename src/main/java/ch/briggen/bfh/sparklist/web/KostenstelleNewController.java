package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kostenstelle;
import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class KostenstelleNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KostenstelleNewController.class);
		
	private KostenstelleRepository custRepo = new KostenstelleRepository();

	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kostenstelle customerDetail = KostenstelleWebHelper.KostenstelleFromWeb(request);
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		log.trace("POST /kostenstelle/new mit customerDetail " + customerDetail);
		
		long id = custRepo.insert(customerDetail);
		
		custRepo.insert(customerDetail);
		
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (id == 0) {
			model.put("isadmin", "true");
			model.put("postAction", "/kostenstelle/new");
			model.put("pfad", "kostenstellenew");
			model.put("customerDetail", new Kostenstelle());
			model.put("falsch", true);
			return new ModelAndView(model, "customerDetailTemplate");
		} else {
		response.redirect("/kostenstelle");
		return null;
		}
		
	}
}


