package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.UserSessionRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class LoginValidationController implements TemplateViewRoute {

    private UserRepository userRepo = new UserRepository();
    private UserSessionRepository userSessionRepo = new UserSessionRepository();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        HashMap<String, Object> model = new HashMap<String, Object>();
        String name = request.queryParams("name");
        String password = request.queryParams("password");
        
    	
        if (userRepo.validateAdminUser(name, password)) {
        	
        	model.put("postAction", "/login/anlegen");
        	model.put("isadmin", "true");
        	response.cookie("/", "admin", request.queryParams("name"), 3600, true);
        	response.cookie("/", "eingeloggt", "true", 3600, true);
        	String i = request.queryParams("name");
        	userSessionRepo.insert(i);
            response.redirect("/");
            
            return null;
            
        }
        
        else if (userRepo.validateUser(name, password)) {
        	model.put("postAction", "/login/anlegen");
        	response.cookie("/", "user", request.queryParams("name"), 3600, true);
        	response.cookie("/", "eingeloggt", "true", 3600, true);
        	String i = request.queryParams("name");
        	userSessionRepo.insert(i);
            response.redirect("/");
            return null;
            
        }
        
        else {
        	model.put("loeschen", userSessionRepo.loeschen());
            model.put("postAction", "/login/validation");
            model.put("UserValidation", new User());
            model.put("authentication", false);
            return new ModelAndView(model, "login");
        }
        
        }
    }
