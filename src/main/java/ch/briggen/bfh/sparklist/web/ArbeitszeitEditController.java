package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Arbeitszeit;
import ch.briggen.bfh.sparklist.domain.ArbeitszeitRepository;
import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ArbeitszeitEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ArbeitszeitEditController.class);
	
	
	
	private ArbeitszeitRepository arbeitszeitRepo = new ArbeitszeitRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		KostenstelleRepository custRepo = new KostenstelleRepository();		
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /arbeitszeit/edit für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/arbeitszeit/new");
			model.put("pfad", "arbeitszeitnew");
			model.put("arbeitszeitDetail", new Arbeitszeit());
			model.put("projekte", custRepo.getAll());
			
		}
		else
		{
			log.trace("GET /arbeitszeit/edit für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/arbeitszeit/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			int id = Integer.parseInt(idString);
			Arbeitszeit i = arbeitszeitRepo.getById(id);
			model.put("arbeitszeitDetail", i);
			model.put("pfad", "arbeitszeitedit");
			model.put("projekte", custRepo.getAll());
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null){
			model.put("isadmin", "true");
			return new ModelAndView(model, "arbeitszeitDetailTemplate");
		}
		
		else {
			return new ModelAndView(model, "arbeitszeitDetailTemplate");
			
		}
	}
	
	
	
}


