package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.BetriebsferienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementBetriebsferienController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementBetriebsferienController.class);

	BetriebsferienRepository repository = new BetriebsferienRepository();
	
	

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Feriens werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAll());
		model.put("listadmin", repository.getAll());
		model.put("pfad", "betriebsferien");
		
		model.put("pfad", "betriebsferien");
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null) {
			model.put("isadmin", "true");
			return new ModelAndView(model, "betriebsferienTemplate");
		}
		
		else {
			return new ModelAndView(model, "betriebsferienTemplate");
			
		}
	}
}
