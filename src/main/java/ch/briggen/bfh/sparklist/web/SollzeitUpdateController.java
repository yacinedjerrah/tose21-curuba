package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Sollzeit;
import ch.briggen.bfh.sparklist.domain.SollzeitRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class SollzeitUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(SollzeitUpdateController.class);
		
	private SollzeitRepository SollzeitRepo = new SollzeitRepository();
	


	/**
	 * Schreibt das geänderte Item zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /item/update
	 * 
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Sollzeit sollzeitDetail = SollzeitWebHelper.sollzeitFromWeb(request);
		
		log.trace("POST /sollzeit/aktualisieren mit sollzeitDetail " + sollzeitDetail);
		
		SollzeitRepo.save(sollzeitDetail);

		response.redirect("/sollzeit");

		return null;
	}
}


