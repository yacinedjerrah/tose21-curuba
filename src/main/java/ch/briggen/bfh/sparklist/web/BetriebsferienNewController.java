package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Betriebsferien;
import ch.briggen.bfh.sparklist.domain.BetriebsferienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BetriebsferienNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BetriebsferienNewController.class);
		
	
	private BetriebsferienRepository betriebsferienRepo = new BetriebsferienRepository();
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Betriebsferien betriebsferienDetail = BetriebsferienWebHelper.betriebsferienFromWeb(request);
		log.trace("POST /betriebsferien/anlegen mit betriebsferienDetail " + betriebsferienDetail);
		
		if (request.cookie("admin") != null) {
			betriebsferienRepo.insert(betriebsferienDetail);
		}
		
		
		
		
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		//response.redirect("/Arbeitszeit?id="+id);
		response.redirect("/betriebsferien");
		return null;
	}
}


