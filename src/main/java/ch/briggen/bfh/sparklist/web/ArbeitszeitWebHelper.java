package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Arbeitszeit;
import spark.Request;

import java.sql.Date;

class ArbeitszeitWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ArbeitszeitWebHelper.class);
	
	public static Arbeitszeit arbeitszeitFromWeb(Request request)
	{
		return new Arbeitszeit(
				Integer.parseInt(request.queryParams("arbeitszeitDetail.id")),
				Integer.parseInt(request.queryParams("arbeitszeitDetail.arbeiterid")),
				request.queryParams("arbeitszeitDetail.eintrag"),
				Float.parseFloat(request.queryParams("arbeitszeitDetail.arbeitszeit")),
				Date.valueOf(request.queryParams("arbeitszeitDetail.datum")));
	}
	
}
