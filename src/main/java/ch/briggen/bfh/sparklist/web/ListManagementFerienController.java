package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.FerienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementFerienController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementFerienController.class);

	FerienRepository repository = new FerienRepository();
	
	

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Feriens werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAll());
		model.put("listadmin", repository.getAllAdmin());
		model.put("pfad", "ferien");
		
		model.put("pfad", "ferien");
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null) {
			model.put("isadmin", "true");
			return new ModelAndView(model, "ferienTemplate");
		}
		
		else {
			return new ModelAndView(model, "ferienTemplate");
			
		}
	}
}
