package ch.briggen.bfh.sparklist.web;



import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Ferien;
import spark.Request;

class FerienWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(FerienWebHelper.class);
	
	public static Ferien ferienFromWeb(Request request)
	{
		return new Ferien(
				Integer.parseInt(request.queryParams("ferienDetail.id")),
				Integer.parseInt(request.queryParams("ferienDetail.arbeiterid")),
				Float.parseFloat(request.queryParams("ferienDetail.ferientage")),
				Date.valueOf(request.queryParams("ferienDetail.von")),
				Date.valueOf(request.queryParams("ferienDetail.bis")),
				Boolean.valueOf(request.queryParams("ferienDetail.genehmigt")));
	}

	public Ferien findByFerienname(String ferienname) {
		return null;
	}
}
