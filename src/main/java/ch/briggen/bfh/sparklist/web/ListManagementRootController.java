	package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ArbeitszeitRepository;
import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	ArbeitszeitRepository repository = new ArbeitszeitRepository();
	KostenstelleRepository custRepo = new KostenstelleRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {		
		//Arbeitszeits werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAllToday());
		model.put("listadmin", repository.getAllTodayAdmin());
		model.put("alle", repository.getAll());
		model.put("alleadmin", repository.getAllAdmin());
		model.put("listmonat", repository.getAllThisMonth());
		model.put("listmonatadmin", repository.getAllThisMonthAdmin());
		model.put("projekte", custRepo.getAll());
		model.put("pfad", "arbeitszeit");
		
		
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null) {
			model.put("isadmin", "true");
			return new ModelAndView(model, "listTemplate");
		}
		
		
		else {
			return new ModelAndView(model, "listTemplate");
		}
	}
}
