package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Ferien;
import ch.briggen.bfh.sparklist.domain.FerienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class FerienEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(FerienEditController.class);
	
	
	
	private FerienRepository ferienRepo = new FerienRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /ferien/bearbeiten für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/ferien/anlegen");
			model.put("pfad", "feriennew");
			model.put("ferienDetail", new Ferien());
		}
		else
		{
			log.trace("GET /ferien/bearbeiten für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/ferien/aktualisieren");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			int id = Integer.parseInt(idString);
			Ferien i = ferienRepo.getById(id);
			model.put("ferienDetail", i);
			model.put("pfad", "ferienedit");
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null){
			model.put("isadmin", "true");
			return new ModelAndView(model, "ferienDetailTemplate");
		}
		
		else {
			return new ModelAndView(model, "ferienDetailTemplate");
			
		}
	}
	
	
	
}


