package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Ferien;
import ch.briggen.bfh.sparklist.domain.FerienRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class FerienNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(FerienNewController.class);
		
	
	private FerienRepository ferienRepo = new FerienRepository();
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Ferien ferienDetail = FerienWebHelper.ferienFromWeb(request);
		log.trace("POST /ferien/anlegen mit ferienDetail 123123" + ferienDetail);
		
		if (request.cookie("admin") != null) {
			ferienRepo.insert(ferienDetail);
		}
		
		else {
			ferienRepo.insertFromUser(ferienDetail);
		}
		
		
		
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		//response.redirect("/Arbeitszeit?id="+id);
		response.redirect("/ferien");
		return null;
	}
}


