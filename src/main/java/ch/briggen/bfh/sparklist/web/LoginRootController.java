package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserSession;
import ch.briggen.bfh.sparklist.domain.UserSessionRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class LoginRootController implements TemplateViewRoute {
	private UserSessionRepository userSessionRepo = new UserSessionRepository();
	

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String idString = request.queryParams("id");
        HashMap<String, Object> model = new HashMap<String, Object>();

        if (request.cookie("eingeloggt") != null) {
        	response.redirect("/");
            return null;
        }
        
        else if(null == idString) {
        	model.put("loeschen", userSessionRepo.loeschen());
            model.put("postAction", "/login/validation");
            model.put("UserValidation", new User());
            model.put("userSessionDetail", new UserSession());
            return new ModelAndView(model, "login");
        }
        else {
        	response.redirect("/");
            return null;
        }

    }

    }
