package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class KostenstelleDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KostenstelleDeleteController.class);
		
	private KostenstelleRepository custRepo = new KostenstelleRepository();
	

	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /kostenstelle/delete mit id " + id);
		
		int intId = Integer.parseInt(id);
		custRepo.delete(intId);
		response.redirect("/kostenstelle");
		return null;
	}
}


