package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Sollzeit;
import ch.briggen.bfh.sparklist.domain.SollzeitRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class SollzeitNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(SollzeitNewController.class);
		
	
	private SollzeitRepository sollzeitRepo = new SollzeitRepository();
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Sollzeit sollzeitDetail = SollzeitWebHelper.sollzeitFromWeb(request);
		log.trace("POST /sollzeit/anlegen mit sollzeit 123123" + sollzeitDetail);
		
		if (request.cookie("admin") != null) {
			sollzeitRepo.insert(sollzeitDetail);
		}

		
		
		
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		//response.redirect("/Arbeitszeit?id="+id);
		response.redirect("/sollzeit");
		return null;
	}
}


