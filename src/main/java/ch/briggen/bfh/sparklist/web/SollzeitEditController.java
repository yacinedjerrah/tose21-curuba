package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Sollzeit;
import ch.briggen.bfh.sparklist.domain.SollzeitRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class SollzeitEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(SollzeitEditController.class);
	
	
	
	private SollzeitRepository sollzeitRepo = new SollzeitRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /sollzeit/bearbeiten für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/sollzeit/anlegen");
			model.put("pfad", "sollzeitnew");
			model.put("sollzeitDetail", new Sollzeit());
		}
		else
		{
			log.trace("GET /sollzeit/bearbeiten für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/sollzeit/aktualisieren");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			int id = Integer.parseInt(idString);
			Sollzeit i = sollzeitRepo.getById(id);
			model.put("sollzeitDetail", i);
			model.put("pfad", "sollzeitedit");
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null){
			model.put("isadmin", "true");
			return new ModelAndView(model, "sollzeitDetailTemplate");
		}
		
		else {
			return new ModelAndView(model, "sollzeitDetailTemplate");
			
		}
	}
	
	
	
}


