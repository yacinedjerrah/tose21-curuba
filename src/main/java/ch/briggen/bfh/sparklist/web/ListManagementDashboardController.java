	package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ArbeitszeitRepository;
import ch.briggen.bfh.sparklist.domain.UserSessionRepository;
import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import ch.briggen.bfh.sparklist.domain.SollzeitRepository;
import ch.briggen.bfh.sparklist.domain.DashboardAZS;
import ch.briggen.bfh.sparklist.domain.DashboardAK;
import ch.briggen.bfh.sparklist.domain.DashboardProfit;
import ch.briggen.bfh.sparklist.domain.DashboardFerientageAlle;
import ch.briggen.bfh.sparklist.domain.DashboardOvertime;
import ch.briggen.bfh.sparklist.domain.DashboardRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!, 
 * 
 * @author M. Briggen
 *
 */
public class ListManagementDashboardController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementDashboardController.class);

	ArbeitszeitRepository repository = new ArbeitszeitRepository();
	KostenstelleRepository custRepo = new KostenstelleRepository();
	UserSessionRepository userSessionRepo = new UserSessionRepository();
	DashboardRepository dashRepo = new DashboardRepository();
	SollzeitRepository SollRepo = new SollzeitRepository();
	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else {
		//Arbeitszeits werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		DashboardAZS p = repository.getAlleArbeitsstunden();
		model.put("allestunden", p);
		DashboardAZS z = repository.getAlleArbeitsstundenMitarbeiter();
		model.put("allestundenmitarbeiter", z);
		//DashboardProfit s = repository.getAlleProfit();
		//model.put("profit", s);
		//DashboardProfit y = repository.getAlleProfitMitarbeiter();
		//model.put("profitmitarbeiter", y);
		DashboardAK m = repository.getAlleKunden();
		model.put("allekunden", m);
		DashboardAK x = repository.getAlleKundenMitarbeiter();
		model.put("allekundenmitarbeiter", x);
		DashboardFerientageAlle n = dashRepo.getAlleFerienMitarbeiter();
		model.put("alleferienmitarbeiter", n);
		DashboardFerientageAlle r = dashRepo.getAktuelleFerienMitarbeiter();
		model.put("aktuelleferienmitarbeiter", r);
		DashboardFerientageAlle q = dashRepo.getRestlicheFerienMitarbeiter();
		model.put("restlicheferienmitarbeiter", q);
		model.put("ferienkalender", dashRepo.getAlleFerienMitarbeiterKalender());
		model.put("betriebsferienkalender", dashRepo.getBetriebsferien());
		model.put("stundenkalender", dashRepo.getArbeitszeitKalender());
		model.put("kundennamen", repository.getAlleKundenNamen());
		model.put("alle", repository.getAll());
		//model.put("profitkunde", repository.DashboardProfitProKunde());
		//model.put("profitkundemitarbeiter", repository.DashboardProfitProKundeMitarbeiter());
		model.put("listmonat", repository.getAllThisMonth());
		model.put("projekte", custRepo.getAll());
		model.put("sollzeiten", SollRepo.getNow());
		DashboardOvertime s = repository.getUeberzeitMitarbeiter();
		model.put("ueberzeit" , s);
		model.put("pfad", "dashboard");
		
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null){
			model.put("isadmin", "true");
			return new ModelAndView(model, "dashboard");
		}
		
		else {
			return new ModelAndView(model, "dashboard");
			
		}
		}
	}
}
