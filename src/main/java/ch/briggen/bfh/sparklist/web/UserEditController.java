package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class UserEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(UserEditController.class);
	
	
	
	private UserRepository userRepo = new UserRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /benutzer/bearbeiten für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/benutzer/anlegen");
			model.put("pfad", "usernew");
			model.put("userDetail", new User());
		}
		else
		{
			log.trace("GET /benutzer/bearbeiten für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/benutzer/aktualisieren");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			int id = Integer.parseInt(idString);
			User i = userRepo.getById(id);
			model.put("userDetail", i);
			model.put("pfad", "useredit");
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		if (request.cookie("eingeloggt") == null) {
			response.redirect("/login");
			return null;
		} else if (request.cookie("admin") != null){
			model.put("isadmin", "true");
			return new ModelAndView(model, "userDetailTemplate");
		}
		else {
			response.redirect("/");
			return null;
		}
		
		
	}
	
	
	
}


