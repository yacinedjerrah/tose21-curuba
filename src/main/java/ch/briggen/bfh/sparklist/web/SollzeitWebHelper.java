package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Sollzeit;
import spark.Request;

class SollzeitWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(SollzeitWebHelper.class);
	
	public static Sollzeit sollzeitFromWeb(Request request)
	{
		return new Sollzeit(
				Integer.parseInt(request.queryParams("sollzeitDetail.id")),
				Integer.parseInt(request.queryParams("sollzeitDetail.jahr")),
				Integer.parseInt(request.queryParams("sollzeitDetail.monat")),
				Integer.parseInt(request.queryParams("sollzeitDetail.arbeitstage")),
				Integer.parseInt(request.queryParams("sollzeitDetail.sollzeit")));
	}
	
}
