package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kostenstelle;
import ch.briggen.bfh.sparklist.domain.KostenstelleRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class KostenstelleUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(KostenstelleUpdateController.class);
		
	private KostenstelleRepository custRepo = new KostenstelleRepository();
	


	/**
	 * Schreibt das geänderte Item zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /item/update
	 * 
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kostenstelle customerDetail = KostenstelleWebHelper.KostenstelleFromWeb(request);
		
		log.trace("POST /kostenstelle/update mit customerDetail " + customerDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		custRepo.save(customerDetail);
		//response.redirect("/arbeitszeit?id="+arbeitszeitDetail.getId());
		response.redirect("/kostenstelle");

		return null;
	}
}


