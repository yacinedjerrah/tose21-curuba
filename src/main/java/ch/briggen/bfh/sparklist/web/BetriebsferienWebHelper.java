package ch.briggen.bfh.sparklist.web;



import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Betriebsferien;
import spark.Request;

class BetriebsferienWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(BetriebsferienWebHelper.class);
	
	public static Betriebsferien betriebsferienFromWeb(Request request)
	{
		return new Betriebsferien(
				Integer.parseInt(request.queryParams("betriebsferienDetail.id")),
				Date.valueOf(request.queryParams("betriebsferienDetail.von")),
				Date.valueOf(request.queryParams("betriebsferienDetail.bis")),
				String.valueOf(request.queryParams("betriebsferienDetail.eintrag")));
	}

}
