package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ArbeitszeitDeleteController;
import ch.briggen.bfh.sparklist.web.ArbeitszeitEditController;
import ch.briggen.bfh.sparklist.web.ArbeitszeitNewController;
import ch.briggen.bfh.sparklist.web.ArbeitszeitUpdateController;
import ch.briggen.bfh.sparklist.web.BetriebsferienDeleteController;
import ch.briggen.bfh.sparklist.web.BetriebsferienEditController;
import ch.briggen.bfh.sparklist.web.BetriebsferienNewController;
import ch.briggen.bfh.sparklist.web.BetriebsferienUpdateController;
import ch.briggen.bfh.sparklist.web.KostenstelleDeleteController;
import ch.briggen.bfh.sparklist.web.KostenstelleEditController;
import ch.briggen.bfh.sparklist.web.KostenstelleNewController;
import ch.briggen.bfh.sparklist.web.KostenstelleUpdateController;
import ch.briggen.bfh.sparklist.web.ListManagementBetriebsferienController;
import ch.briggen.bfh.sparklist.web.UserDeleteController;
import ch.briggen.bfh.sparklist.web.UserEditController;
import ch.briggen.bfh.sparklist.web.UserNewController;
import ch.briggen.bfh.sparklist.web.UserUpdateController;
import ch.briggen.bfh.sparklist.web.LoginValidationController;
import ch.briggen.bfh.sparklist.web.SollzeitEditController;
import ch.briggen.bfh.sparklist.web.SollzeitNewController;
import ch.briggen.bfh.sparklist.web.SollzeitUpdateController;
import ch.briggen.bfh.sparklist.web.LoginRootController;
import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.ListManagementSollzeitController;
import ch.briggen.bfh.sparklist.web.ListManagementUserController;
import ch.briggen.bfh.sparklist.web.ListManagementDashboardController;
import ch.briggen.bfh.sparklist.web.ListManagementKostenstelleController;
import ch.briggen.bfh.sparklist.web.FerienDeleteController;
import ch.briggen.bfh.sparklist.web.FerienEditController;
import ch.briggen.bfh.sparklist.web.FerienNewController;
import ch.briggen.bfh.sparklist.web.FerienUpdateController;
import ch.briggen.bfh.sparklist.web.ListManagementFerienController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/arbeitszeit", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/", new ListManagementDashboardController(), new UTF8ThymeleafTemplateEngine());
		get("/benutzer", new ListManagementUserController(), new UTF8ThymeleafTemplateEngine());
		get("/arbeitszeit/edit", new ArbeitszeitEditController(), new UTF8ThymeleafTemplateEngine());
		post("/arbeitszeit/update", new ArbeitszeitUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/arbeitszeit/delete", new ArbeitszeitDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/arbeitszeit/new", new ArbeitszeitNewController(), new UTF8ThymeleafTemplateEngine());
		get("/benutzer/bearbeiten", new UserEditController(), new UTF8ThymeleafTemplateEngine());
		post("/benutzer/aktualisieren", new UserUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/benutzer/loeschen", new UserDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/benutzer/anlegen", new UserNewController(), new UTF8ThymeleafTemplateEngine());
		post("/login/validation", new LoginValidationController(), new UTF8ThymeleafTemplateEngine());
		get("/login", new LoginRootController(), new UTF8ThymeleafTemplateEngine());
		get("/kostenstelle/bearbeiten", new KostenstelleEditController(), new UTF8ThymeleafTemplateEngine());
		post("/kostenstelle/update", new KostenstelleUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/kostenstelle/delete", new KostenstelleDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/kostenstelle/new", new KostenstelleNewController(), new UTF8ThymeleafTemplateEngine());
		get("/kostenstelle", new ListManagementKostenstelleController(), new UTF8ThymeleafTemplateEngine());
		get("/ferien", new ListManagementFerienController(), new UTF8ThymeleafTemplateEngine());
		get("/ferien/bearbeiten", new FerienEditController(), new UTF8ThymeleafTemplateEngine());
		post("/ferien/aktualisieren", new FerienUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/ferien/loeschen", new FerienDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/ferien/anlegen", new FerienNewController(), new UTF8ThymeleafTemplateEngine());
		get("/sollzeit", new ListManagementSollzeitController(), new UTF8ThymeleafTemplateEngine());
		post("/sollzeit/anlegen", new SollzeitNewController(), new UTF8ThymeleafTemplateEngine());
		get("/sollzeit/bearbeiten", new SollzeitEditController(), new UTF8ThymeleafTemplateEngine());
		post("/sollzeit/aktualisieren", new SollzeitUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/betriebsferien", new ListManagementBetriebsferienController(), new UTF8ThymeleafTemplateEngine());
		post("/betriebsferien/anlegen", new BetriebsferienNewController(), new UTF8ThymeleafTemplateEngine());
		get("/betriebsferien/bearbeiten", new BetriebsferienEditController(), new UTF8ThymeleafTemplateEngine());
		post("/betriebsferien/aktualisieren", new BetriebsferienUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/betriebsferien/loeschen", new BetriebsferienDeleteController(), new UTF8ThymeleafTemplateEngine());
	}

}
