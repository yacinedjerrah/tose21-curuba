package ch.briggen.bfh.sparklist.domain;





public class Kostenstelle {
	
	private int id;
	private String name;
	private int nr;
	private String ort;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Kostenstelle()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Kostenstelle(int id, String name, int nr, String ort)
	{
		this.id = id;
		this.name = name;
		this.nr = nr;
		this.ort = ort;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@Override
	public String toString() {
		return String.format("Benutzer:{id: %d; name: %s; nr: %s; ort: %s;}", id, name, nr, ort);
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}
	

}
