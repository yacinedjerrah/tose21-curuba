package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BetriebsferienRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ArbeitszeitRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<Betriebsferien> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, von, bis, eintrag FROM TBL_BETRIEBSFERIEN ORDER BY von DESC ");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BETRIEBSFERIEN(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Feriens. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public Betriebsferien getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, von, bis, eintrag from tbl_betriebsferien where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BETRIEBSFERIEN(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Betriebsferiens by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(Betriebsferien i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_betriebsferien set von=?, bis=?, eintrag=? where id=?");
			stmt.setDate(1, i.getVon());
			stmt.setDate(2, i.getBis());
			stmt.setString(3, i.getEintrag());
			stmt.setInt(4, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Betriebsferiens " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from tbl_betriebsferien where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Feriens by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(Betriebsferien i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_betriebsferien (von, bis, eintrag) values (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setDate(1, i.getVon());
			stmt.setDate(2, i.getBis());
			stmt.setString(3, i.getEintrag());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
	

	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Betriebsferien> mapTBL_BETRIEBSFERIEN(ResultSet rs) throws SQLException 
	{
		LinkedList<Betriebsferien> list = new LinkedList<Betriebsferien>();
		while(rs.next())
		{
			Betriebsferien f = new Betriebsferien(rs.getInt("id"),rs.getDate("von"),rs.getDate("bis"),rs.getString("eintrag"));
			list.add(f);
		}
		return list;
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public boolean login(String username, String password) {
		return false;
		

	}


}