package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardFerientageAlle {

	private float anzahlferientage;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardFerientageAlle()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardFerientageAlle(float anzahlferientage)
	{
		this.anzahlferientage = anzahlferientage;
	}

	
	public float getAnzahlArbeitsstunden() {
		return anzahlferientage;
	}

	public void setAnzahlArbeitsstunden(float anzahlferientage) {
		this.anzahlferientage = anzahlferientage;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%.1f", anzahlferientage);
	}
	

}
