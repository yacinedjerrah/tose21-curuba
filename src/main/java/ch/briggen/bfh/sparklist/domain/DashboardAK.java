package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardAK {

	private int anzahl_kunden;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardAK()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardAK(int anzahl_kunden)
	{
		this.anzahl_kunden = anzahl_kunden;
	}

	
	public float getAnzahlKunden() {
		return anzahl_kunden;
	}

	public void setAnzahlKunden(int anzahl_kunden) {
		this.anzahl_kunden = anzahl_kunden;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%d", anzahl_kunden);
	}
	

}
