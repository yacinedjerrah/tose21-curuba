package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Dashboard {
	
	private int id;
	private int anzahl_kunden;
	private float anzahl_arbeitsstunden;
	private float anzahl_stunden_klaus;
	private float anzahl_stunden_angela;
	private float anzahl_stunden_donald;
	private float anzahl_stunden_breel;
	private float anzahl_stunden_prokunde;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Dashboard()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Dashboard(int id, int anzahl_kunden, float anzahl_arbeitsstunden, float anzahl_stunden_klaus, 
			float anzahl_stunden_angela, float anzahl_stunden_donald, float anzahl_stunden_breel, float anzahl_stunden_prokunde)
	{
		this.id = id;
		this.anzahl_kunden = anzahl_kunden;
		this.anzahl_arbeitsstunden = anzahl_arbeitsstunden;
		this.anzahl_stunden_klaus = anzahl_stunden_klaus;
		this.anzahl_stunden_angela = anzahl_stunden_angela;
		this.anzahl_stunden_donald = anzahl_stunden_donald;
		this.anzahl_stunden_breel = anzahl_stunden_breel;
		this.anzahl_stunden_prokunde = anzahl_stunden_prokunde;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getAnzahlKunden() {
		return anzahl_kunden;
	}
	
	public void setAnzahlKunden(int anzahl_kunden) {
		this.anzahl_kunden = anzahl_kunden;
	}
	
	public float getAnzahlArbeitsstunden() {
		return anzahl_arbeitsstunden;
	}

	public void setAnzahlArbeitsstunden(float anzahl_arbeitsstunden) {
		this.anzahl_arbeitsstunden = anzahl_arbeitsstunden;
	}
	
	public float getAnzahlArbeitsstundenKlaus() {
		return anzahl_stunden_klaus;
	}

	public void setAnzahlArbeitsstundenKlaus(float anzahl_stunden_klaus) {
		this.anzahl_stunden_klaus = anzahl_stunden_klaus;
	}
	
	public float getAnzahlArbeitsstundenAngela() {
		return anzahl_stunden_angela;
	}

	public void setAnzahlArbeitsstundenAngela(float anzahl_stunden_angela) {
		this.anzahl_stunden_angela = anzahl_stunden_angela;
	}
	
	public float getAnzahlArbeitsstundenDonald() {
		return anzahl_stunden_donald;
	}

	public void setAnzahlArbeitsstundenDonald(float anzahl_stunden_donald) {
		this.anzahl_stunden_donald = anzahl_stunden_donald;
	}
	
	public float getAnzahlArbeitsstundenBreel() {
		return anzahl_stunden_breel;
	}

	public void setAnzahlArbeitsstundenBreel(float anzahl_stunden_breel) {
		this.anzahl_stunden_breel = anzahl_stunden_breel;
	}
	
	public float getAnzahlArbeitsstundenProKunde() {
		return anzahl_stunden_prokunde;
	}

	public void setAnzahlArbeitsstundenProKunde(float anzahl_stunden_prokunde) {
		this.anzahl_stunden_prokunde = anzahl_stunden_prokunde;
	}

	
	
	
	
	@Override
	public String toString() {
		return String.format("Dashboard:{id: %d; anzahl_kunden: %d anzahl_arbeitsstunden: %f; anzahl_stunden_klaus: %f; anzahl_stunden_angela: %f; anzahl_stunden_donald: %f; anzahl_stunden_breel: %f; anzahl_stunden_prokunde: %f}", id, anzahl_kunden, anzahl_arbeitsstunden, anzahl_stunden_klaus, anzahl_stunden_angela, anzahl_stunden_donald, anzahl_stunden_breel, anzahl_stunden_prokunde);
	}
	

}
