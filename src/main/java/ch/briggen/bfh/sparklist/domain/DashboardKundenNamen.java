package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardKundenNamen {

	private String[] kundenname;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardKundenNamen()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardKundenNamen(String[] kundenname)
	{
		this.kundenname = kundenname;
	}

	
	public String[] getKundennamen() {
		return kundenname;
	}

	public void setKundennamen(String[] kundenname) {
		this.kundenname = kundenname;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%s", (Object[])kundenname);
	}
	

}
