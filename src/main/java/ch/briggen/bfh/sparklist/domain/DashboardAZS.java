package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardAZS {

	private float anzahl_arbeitsstunden;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardAZS()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardAZS(float anzahl_arbeitsstunden)
	{
		this.anzahl_arbeitsstunden = anzahl_arbeitsstunden;
	}

	
	public float getAnzahlArbeitsstunden() {
		return anzahl_arbeitsstunden;
	}

	public void setAnzahlArbeitsstunden(float anzahl_arbeitsstunden) {
		this.anzahl_arbeitsstunden = anzahl_arbeitsstunden;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%.2f", anzahl_arbeitsstunden);
	}
	

}
