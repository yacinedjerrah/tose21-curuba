package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UserRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ArbeitszeitRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<User> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, password, pensum, ferien from tbl_benutzer");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<User> getUserID()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id from tbl_benutzer");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public boolean validateUser(String name, String password) {
        log.trace("getAll");
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT name, password from tbl_benutzer WHERE name=? AND password=?");
            stmt.setString(1, name);
            stmt.setString(2, password);
            System.out.println(stmt);

            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) {
                //TODO: Show message in browser when validation fails
                return false;
            } else {
                System.out.println("success");
                return true;
            }
        } catch (SQLException e) {
            String msg = "SQL error while User Validation.";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }
	
	public boolean validateAdminUser(String name, String password) {
        log.trace("getAll");
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT name, password from tbl_benutzer WHERE name=? AND password=? AND admin=true");
            stmt.setString(1, name);
            stmt.setString(2, password);
            System.out.println(stmt);

            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) {
                //TODO: Show message in browser when validation fails
                return false;
            } else {
                System.out.println("success");
                return true;
            }
        } catch (SQLException e) {
            String msg = "SQL error while User Validation.";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }


	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public static Collection<User> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, password, pensum, ferien from tbl_benutzer where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Users by Name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public User getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, password, pensum, ferien from tbl_benutzer where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Users by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(User i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_benutzer set name=?, password=?, pensum = ?, ferien = ? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getPassword());
			stmt.setInt(3, i.getPensum());
			stmt.setFloat(4, i.getFerien());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Users " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from tbl_benutzer where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Users by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(User i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_benutzer (name, password, pensum, ferien) values (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getPassword());
			stmt.setInt(3, i.getPensum());
			stmt.setFloat(4, i.getFerien());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<User> mapTBL_BENUTZER(ResultSet rs) throws SQLException 
	{
		LinkedList<User> list = new LinkedList<User>();
		while(rs.next())
		{
			User i = new User(rs.getInt("id"),rs.getString("name"),rs.getString("password"),rs.getInt("pensum"),rs.getFloat("ferien"));
			list.add(i);
		}
		return list;
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public boolean login(String username, String password) {
		return false;
		

	}

	


}