package ch.briggen.bfh.sparklist.domain;





public class UserSessionId {
	
	private int userid;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public UserSessionId()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public UserSessionId(int userid)
	{
		this.userid = userid;
	}

	
	public int getId() {
		return userid;
	}
	
	public void setId(int userid) {
		this.userid = userid;
	}
	

	
	
	@Override
	public String toString() {
		return String.format("UserSession:{userid: %d;}",userid);
	}

}
