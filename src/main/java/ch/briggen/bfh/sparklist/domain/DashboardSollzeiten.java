package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardSollzeiten {

	private float sollzeiten;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardSollzeiten()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardSollzeiten(float sollzeiten)
	{
		this.sollzeiten = sollzeiten;
	}

	
	public float getAnzahlSollstunden() {
		return sollzeiten;
	}

	public void setAnzahlSollzeiten(float sollzeiten) {
		this.sollzeiten = sollzeiten;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%.1f", sollzeiten);
	}
	

}
