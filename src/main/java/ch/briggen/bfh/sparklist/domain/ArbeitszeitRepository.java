package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import spark.Request;
import spark.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class ArbeitszeitRepository {
	
	private final Logger log = LoggerFactory.getLogger(ArbeitszeitRepository.class);

	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<Arbeitszeit> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select tbl_arbeitszeit.id, tbl_arbeitszeit.arbeiterid, tbl_session.name, tbl_benutzer.name, tbl_arbeitszeit.eintrag, tbl_arbeitszeit.arbeitszeit, tbl_arbeitszeit.datum from tbl_arbeitszeit INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name;");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	
	public Collection<Arbeitszeit> getAllAdmin()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, eintrag, arbeitszeit, datum from tbl_arbeitszeit");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<Arbeitszeit> getAllToday()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select tbl_arbeitszeit.id, tbl_arbeitszeit.arbeiterid, tbl_session.name, tbl_benutzer.name, tbl_arbeitszeit.eintrag, tbl_arbeitszeit.arbeitszeit, tbl_arbeitszeit.datum from tbl_arbeitszeit INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name where tbl_arbeitszeit.datum=CURDATE();");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<Arbeitszeit> getAllTodayAdmin()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, eintrag, arbeitszeit, datum from tbl_arbeitszeit where datum=CURDATE();");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<Arbeitszeit> getAllThisMonth()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select tbl_arbeitszeit.id, tbl_arbeitszeit.arbeiterid, tbl_session.name, tbl_benutzer.name, tbl_arbeitszeit.eintrag, tbl_arbeitszeit.arbeitszeit, tbl_arbeitszeit.datum from tbl_arbeitszeit INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name where MONTH(datum)= MONTH(CURDATE())");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<Arbeitszeit> getAllThisMonthAdmin()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, eintrag, arbeitszeit, datum from tbl_arbeitszeit where MONTH(datum)= MONTH(CURDATE())");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public DashboardAZS getAlleArbeitsstunden()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select sum(arbeitszeit) from tbl_arbeitszeit");
			ResultSet rs = stmt.executeQuery();
			return mapSumme(rs).iterator().next();	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Arbeitsstunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public DashboardAZS getAlleArbeitsstundenMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select sum(arbeitszeit) from tbl_arbeitszeit INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name;");
			ResultSet rs = stmt.executeQuery();
			return mapSumme(rs).iterator().next();	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Arbeitsstunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public DashboardAK getAlleKunden()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select count(id) from tbl_kostenstelle where name != 'Unproduktiv'");
			ResultSet rs = stmt.executeQuery();
			return mapKunden(rs).iterator().next();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Kunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public DashboardAK getAlleKundenMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select count(tbl_kostenstelle.id) from tbl_kostenstelle INNER JOIN tbl_arbeitszeit ON tbl_kostenstelle.name=tbl_arbeitszeit.eintrag INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name where tbl_kostenstelle.name != 'Unproduktiv'");
			ResultSet rs = stmt.executeQuery();
			return mapKundenMA(rs).iterator().next();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Kunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<DashboardKundenNamen> getAlleKundenNamen()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select name from tbl_kostenstelle where name != 'Unproduktiv'");
			ResultSet rs = stmt.executeQuery();
			return mapKundenName(rs);
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Kunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	public DashboardOvertime getUeberzeitMitarbeiter()  { 
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{


			LocalDate now = LocalDate.now().withDayOfMonth(1).minusDays(1);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyy-MM-dd");
			String ldlm = formatter.format(now);//get last day of the month before 
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int month = localDate.getMonthValue();
			int year = localDate.getYear();
			PreparedStatement stmt = conn.prepareStatement("select (select sum(arbeitszeit) from tbl_arbeitszeit where tbl_arbeitszeit.datum <= ?) - (select sum(tbl_sollzeit.zeit) from  tbl_sollzeit where tbl_sollzeit.monat <= ? and tbl_sollzeit.jahr = ?)  AS arbeitszeit");
			stmt.setString(1, ldlm);
			stmt.setInt(2, month -1);
			stmt.setInt(3, year);
			ResultSet rs = stmt.executeQuery();
			return mapOvertime(rs).iterator().next();

		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Arbeitsstunden. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	/*public DashboardProfit getAlleProfit()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT sum(a.arbeitszeit)*180 AS summe FROM tbl_arbeitszeit AS a INNER JOIN tbl_kostenstelle AS b ON (b.name= a.eintrag) WHERE b.name!='Unproduktiv';");
			ResultSet rs = stmt.executeQuery();
			return mapProfit(rs).iterator().next();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Profit.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public DashboardProfit getAlleProfitMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT sum(a.arbeitszeit)*180 AS summe FROM tbl_arbeitszeit AS a INNER JOIN tbl_kostenstelle AS b ON (b.name= a.eintrag) INNER JOIN tbl_benutzer ON a.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE b.name!='Unproduktiv'");
			ResultSet rs = stmt.executeQuery();
			return mapProfit(rs).iterator().next();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving sum of all Profit.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<DashboardProfitProKunde> DashboardProfitProKunde()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT  eintrag AS kunde, sum(arbeitszeit)*180 AS profit FROM tbl_arbeitszeit WHERE eintrag != 'Unproduktiv' GROUP BY eintrag;");
			ResultSet rs = stmt.executeQuery();
			return mapProfitProKunde(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<DashboardProfitProKunde> DashboardProfitProKundeMitarbeiter()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT eintrag AS kunde, sum(arbeitszeit)*180 AS profit FROM tbl_arbeitszeit  INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE eintrag != 'Unproduktiv' GROUP BY eintrag;");
			ResultSet rs = stmt.executeQuery();
			return mapProfitProKunde(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Arbeitszeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}


	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Arbeitszeit> getByEintrag(String eintrag) {
		log.trace("getByEintrag " + eintrag);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, eintrag, arbeitszeit, datum from tbl_arbeitszeit where eintrag=?");
			stmt.setString(1, eintrag);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Arbeitszeits by eintrag " + eintrag;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public Arbeitszeit getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, eintrag, arbeitszeit, datum from TBL_ARBEITSZEIT where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_ARBEITSZEIT(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Arbeitszeits by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(Arbeitszeit i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update TBL_ARBEITSZEIT set eintrag=?, arbeiterid=?, arbeitszeit=?, datum=? where id=?");
			stmt.setString(1, i.getEintrag());
			stmt.setInt(2, i.getArbeiterid());
			stmt.setFloat(3, i.getArbeitszeit());
			stmt.setDate(4, i.getDatum());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Arbeitszeit " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	

	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from TBL_ARBEITSZEIT where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Arbeitszeits by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(Arbeitszeit i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into TBL_ARBEITSZEIT (arbeiterid, eintrag, arbeitszeit, datum) values (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, i.getArbeiterid());
			stmt.setString(2, i.getEintrag());
			stmt.setFloat(3, i.getArbeitszeit());
			stmt.setDate(4, i.getDatum());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
public int insertFromUser(Arbeitszeit i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into TBL_ARBEITSZEIT (arbeiterid, eintrag, arbeitszeit, datum) values ((select tbl_benutzer.id from tbl_session INNER JOIN tbl_benutzer ON tbl_session.name=tbl_benutzer.name),?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			//stmt.setInt(1, i.getArbeiterid());
			stmt.setString(1, i.getEintrag());
			stmt.setFloat(2, i.getArbeitszeit());
			stmt.setDate(3, i.getDatum());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting Arbeitszeit " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Arbeitszeit> mapTBL_ARBEITSZEIT(ResultSet rs) throws SQLException 
	{
		LinkedList<Arbeitszeit> list = new LinkedList<Arbeitszeit>();
		while(rs.next())
		{
			Arbeitszeit i = new Arbeitszeit(rs.getInt("id"),rs.getInt("arbeiterid"),rs.getString("eintrag"),rs.getFloat("arbeitszeit"),rs.getDate("datum"));

			list.add(i);
		}
		return list;
	}
	
	private static Collection<DashboardAZS> mapSumme(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardAZS> list = new LinkedList<DashboardAZS>();
		while(rs.next())
		{
			DashboardAZS p = new DashboardAZS(rs.getFloat("sum(arbeitszeit)"));
			list.add(p);
		}
		return list;
	}
	
	private static Collection<DashboardKundenNamen> mapKundenName(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardKundenNamen> list = new LinkedList<DashboardKundenNamen>();
		while(rs.next())
		{
			DashboardKundenNamen y = new DashboardKundenNamen();
			list.add(y);
		}
		return list;
	}
	
	private static Collection<DashboardProfitProKunde> mapProfitProKunde(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardProfitProKunde> list = new LinkedList<DashboardProfitProKunde>();
		while(rs.next())
		{
			DashboardProfitProKunde k = new DashboardProfitProKunde(rs.getString("kunde"),rs.getInt("profit"));
			list.add(k);
		}
		return list;
	}
	
	private static Collection<DashboardAK> mapKunden(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardAK> list = new LinkedList<DashboardAK>();
		while(rs.next())
		{
			DashboardAK m = new DashboardAK(rs.getInt("count(id)"));
			list.add(m);
		}
		return list;
	}
	
	private static Collection<DashboardAK> mapKundenMA(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardAK> list = new LinkedList<DashboardAK>();
		while(rs.next())
		{
			DashboardAK m = new DashboardAK(rs.getInt("count(tbl_kostenstelle.id)"));
			list.add(m);
		}
		return list;
	}
	
	/*private static Collection<DashboardProfit> mapProfit(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardProfit> list = new LinkedList<DashboardProfit>();
		while(rs.next())
		{
			DashboardProfit s = new DashboardProfit(rs.getFloat("summe"));
			list.add(s);
		}
		return list;
	}*/

	private static Collection<DashboardOvertime> mapOvertime(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardOvertime> list = new LinkedList<DashboardOvertime>();
		while(rs.next())
		{
			DashboardOvertime m = new DashboardOvertime(rs.getInt("arbeitszeit"));
			list.add(m);
		}
		return list;
	}
}
