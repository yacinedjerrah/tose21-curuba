package ch.briggen.bfh.sparklist.domain;





public class Sollzeit {
	
	private int id;
	private int jahr;
	private int monat;
	private int arbeitstage;
	private int sollzeit;



	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Sollzeit()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Sollzeit(int id, int jahr, int monat, int arbeitstage, int sollzeit)
	{
		this.id = id;
		this.jahr = jahr;
		this.monat = monat;
		this.arbeitstage = arbeitstage;
		this.sollzeit = sollzeit;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getYear() {
		return jahr;
	}
	
	public void setYear(int jahr) {
		this.jahr = jahr;
	}
	
	public int getMonth() {
		return monat;
	}

	public void setMonth(int monat) {
		this.monat = monat;
	}
	
	public int getSollzeit() {
		return sollzeit;
	}

	public void setSollzeit(int sollzeit) {
		this.sollzeit = sollzeit;
	}
	
	public int getWorkdays() {
		return arbeitstage;
	}

	public void setWorkdays(int arbeitstage) {
		this.arbeitstage = arbeitstage;
	}



	
	
	
	@Override
	public String toString() {
		return String.format("Benutzer:{id: %d; jahr: %d; monat: %d; arbeitstage: %d ; sollzeit: %d;}", id, jahr, monat, arbeitstage, sollzeit);
	}
	

}
