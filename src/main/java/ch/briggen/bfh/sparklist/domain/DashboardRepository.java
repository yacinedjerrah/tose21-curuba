package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DashboardRepository {
	
	private final Logger log = LoggerFactory.getLogger(DashboardRepository.class);
	
	
	public DashboardFerientageAlle getAlleFerienMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT ferien FROM tbl_benutzer INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE tbl_session.name=tbl_benutzer.name");
			ResultSet rs = stmt.executeQuery();
			return mapFerien(rs).iterator().next();
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	public DashboardFerientageAlle getAktuelleFerienMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT sum(ferientage) AS ferien FROM tbl_ferien INNER JOIN tbl_benutzer ON tbl_ferien.arbeiterid = tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE tbl_session.name=tbl_benutzer.name");
			ResultSet rs = stmt.executeQuery();
			return mapFerien(rs).iterator().next();
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	public DashboardFerientageAlle getRestlicheFerienMitarbeiter()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT tbl_benutzer.ferien - sum(tbl_ferien.ferientage) AS ferien FROM tbl_ferien INNER JOIN tbl_benutzer ON tbl_ferien.arbeiterid = tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE tbl_session.name=tbl_benutzer.name");
			ResultSet rs = stmt.executeQuery();
			return mapFerien(rs).iterator().next();
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	public Collection<DashboardCalendarFerien> getAlleFerienMitarbeiterKalender()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT tbl_benutzer.name,tbl_ferien.von, tbl_ferien.bis FROM tbl_ferien LEFT JOIN tbl_benutzer ON tbl_ferien.arbeiterid=tbl_benutzer.id");
			ResultSet rs = stmt.executeQuery();
			return mapFerienKalender(rs);
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	public Collection<DashboardBetriebsferien> getBetriebsferien()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select eintrag, von, bis from tbl_betriebsferien;");
			ResultSet rs = stmt.executeQuery();
			return mapBetriebsferienKalender(rs);
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	public Collection<DashboardCalendarMeineArbeitszeit> getArbeitszeitKalender()  {
		log.trace("getAlleArbeitsstunden");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT concat(tbl_arbeitszeit.arbeitszeit, ' ', tbl_arbeitszeit.eintrag)  AS auswahl, tbl_arbeitszeit.datum, tbl_arbeitszeit.datum FROM tbl_arbeitszeit  INNER JOIN tbl_benutzer ON tbl_arbeitszeit.arbeiterid = tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name WHERE tbl_session.name=tbl_benutzer.name");
			ResultSet rs = stmt.executeQuery();
			return mapArbeitszeitKalender(rs);
		}
		catch(SQLException e)
		{
			return null;
		}
	}
	
	private static Collection<DashboardFerientageAlle> mapFerien(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardFerientageAlle> list = new LinkedList<DashboardFerientageAlle>();
		while(rs.next())
		{
			DashboardFerientageAlle m = new DashboardFerientageAlle(rs.getFloat("ferien"));
			list.add(m);
		}
		return list;
	}
	
	private static Collection<DashboardCalendarFerien> mapFerienKalender(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardCalendarFerien> list = new LinkedList<DashboardCalendarFerien>();
		while(rs.next())
		{
			DashboardCalendarFerien k = new DashboardCalendarFerien(rs.getString("name"), rs.getString("von"), rs.getString("bis"));
			list.add(k);
		}
		return list;
	}
	private static Collection<DashboardBetriebsferien> mapBetriebsferienKalender(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardBetriebsferien> list = new LinkedList<DashboardBetriebsferien>();
		while(rs.next())
		{
			DashboardBetriebsferien b = new DashboardBetriebsferien(rs.getString("eintrag"), rs.getString("von"), rs.getString("bis"));
			list.add(b);
		}
		return list;
	}
	private static Collection<DashboardCalendarMeineArbeitszeit> mapArbeitszeitKalender(ResultSet rs) throws SQLException 
	{
		LinkedList<DashboardCalendarMeineArbeitszeit> list = new LinkedList<DashboardCalendarMeineArbeitszeit>();
		while(rs.next())
		{
			DashboardCalendarMeineArbeitszeit k = new DashboardCalendarMeineArbeitszeit(rs.getString("auswahl"),rs.getString("datum"), rs.getString("datum"));
			list.add(k);
		}
		return list;
	}
}
