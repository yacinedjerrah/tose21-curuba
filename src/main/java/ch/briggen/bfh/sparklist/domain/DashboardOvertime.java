package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardOvertime {

	private double arbeitszeit;


	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardOvertime()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardOvertime(double arbeitszeit )
	{
		this.arbeitszeit = arbeitszeit;

	}

	
	public double getAnzahlSollstunden() {
		return arbeitszeit;
	}

	public void setAnzahlSollzeiten(float arbeitszeit) {
		this.arbeitszeit = arbeitszeit;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("%s", arbeitszeit);
	}
	

}
