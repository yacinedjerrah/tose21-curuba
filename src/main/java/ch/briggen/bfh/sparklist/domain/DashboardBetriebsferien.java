package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit title und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardBetriebsferien {
	
	private String title;
	private String start;
	private String end;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardBetriebsferien()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param title title des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardBetriebsferien(String title, String start, String end)
	{
		this.title = title;
		this.start = start;
		this.end = end;
	}

	
	public String gettitle() {
		return title;
	}
	
	public String getstart() {
		return start;
	}
	
	public String getend() {
		return end;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("title:'%s', start:'%s', end:'%s'", title, start, end);
	}
	

}
