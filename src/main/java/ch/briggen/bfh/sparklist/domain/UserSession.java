package ch.briggen.bfh.sparklist.domain;





public class UserSession {
	
	private int id;
	private String name;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public UserSession()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public UserSession(int id, String name)
	{
		this.id = id;
		this.name = name;
	}

	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	@Override
	public String toString() {
		return String.format("UserSession:{id: %d; name: %s;}",name,id);
	}

}
