package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KostenstelleRepository {
	
	private final static Logger log = LoggerFactory.getLogger(KostenstelleRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<Kostenstelle> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, nr, ort from tbl_kostenstelle");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_KOSTENSTELLE(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Customers. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public static Collection<Kostenstelle> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, nr, ort from tbl_kostenstelle where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_KOSTENSTELLE(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Customer by Name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public Kostenstelle getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, nr, ort from tbl_kostenstelle where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_KOSTENSTELLE(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Customer by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(Kostenstelle i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_kostenstelle set name=?, nr=?, ort = ? where id=?");
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getNr());
			stmt.setString(3, i.getOrt());
			stmt.setInt(4, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Customer " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from tbl_kostenstelle where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Customers by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(Kostenstelle i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_kostenstelle (name, nr, ort) values (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getNr());
			stmt.setString(3, i.getOrt());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Kostenstelle> mapTBL_KOSTENSTELLE(ResultSet rs) throws SQLException 
	{
		LinkedList<Kostenstelle> list = new LinkedList<Kostenstelle>();
		while(rs.next())
		{
			Kostenstelle i = new Kostenstelle(rs.getInt("id"),rs.getString("name"),rs.getInt("nr"),rs.getString("ort"));
			list.add(i);
		}
		return list;
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */



}