package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SollzeitRepository {
	
	private final static Logger log = LoggerFactory.getLogger(SollzeitRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<Sollzeit> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, jahr, monat, arbeitstage, zeit from tbl_sollzeit ");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_SOLLZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Sollzeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	public DashboardSollzeiten getNow() {
		log.trace("getNow");
		try(Connection conn = getConnection())
		{
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int month = localDate.getMonthValue();
			int year = localDate.getYear();
			PreparedStatement stmt = conn.prepareStatement("select zeit from tbl_sollzeit where jahr = ? AND monat = ?");
			stmt.setInt(1, year);
			stmt.setInt(2, month);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_DashSOLLZEIT(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Sollzeiten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	private static Collection<DashboardSollzeiten> mapTBL_DashSOLLZEIT(ResultSet rs) throws SQLException {
		LinkedList<DashboardSollzeiten> list = new LinkedList<DashboardSollzeiten>();

		while(rs.next())
		{
			DashboardSollzeiten s = new DashboardSollzeiten(rs.getInt("zeit"));
			
			list.add(s);
		}

		return list;
	}


	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public static Collection<Sollzeit> getByYear(int jahr) {
		log.trace("getByYear " + jahr);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, jahr, monat, arbeitstage, zeit from tbl_sollzeit where jahr=?");
			stmt.setInt(1, jahr);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_SOLLZEIT(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Sollzeiten by jahr" + jahr;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public Sollzeit getById(int id) {
		log.trace("getById " + id);
		

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, jahr, monat, arbeitstage, zeit from tbl_sollzeit where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_SOLLZEIT(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Sollzeiten by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(Sollzeit i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_sollzeit set jahr=?, monat=?, arbeitstage=?, zeit=? where id=?");
			stmt.setInt(1, i.getYear());
			stmt.setInt(2, i.getMonth());
			stmt.setInt(3, i.getWorkdays());
			stmt.setInt(4, i.getSollzeit());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Sollzeiten " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(Sollzeit i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_sollzeit (jahr, monat, arbeitstage, zeit) values (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, i.getYear());
			stmt.setInt(2, i.getMonth());
			stmt.setInt(3, i.getWorkdays());
			stmt.setInt(4, i.getSollzeit());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	

	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Sollzeit> mapTBL_SOLLZEIT(ResultSet rs) throws SQLException 
	{
		LinkedList<Sollzeit> list = new LinkedList<Sollzeit>();
		while(rs.next())
		{
			Sollzeit s = new Sollzeit(rs.getInt("id"),rs.getInt("jahr"),rs.getInt("monat"),rs.getInt("arbeitstage"),rs.getInt("zeit"));
			list.add(s);
		}
		return list;
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public boolean login(String username, String password) {
		return false;
		

	}



}