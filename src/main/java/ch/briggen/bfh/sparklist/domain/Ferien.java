package ch.briggen.bfh.sparklist.domain;

import java.sql.Date;




public class Ferien {
	
	private int id;
	private int arbeiterid;
	private float ferientage;
	private Date von;
	private Date bis;
	private boolean genehmigt;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Ferien()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Ferien(int id, int arbeiterid, float ferientage, Date von, Date bis, boolean genehmigt)
	{
		this.id = id;
		this.arbeiterid = arbeiterid;
		this.ferientage = ferientage;
		this.von = von;
		this.bis = bis;
		this.genehmigt = genehmigt;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getArbeiterId() {
		return arbeiterid;
	}
	
	public void setArbeiterId(int arbeiterid) {
		this.arbeiterid = arbeiterid;
	}
	
	public float getFerientage() {
		return ferientage;
	}

	public void setFerientage(float ferientage) {
		this.ferientage = ferientage;
	}
	
	public Date getVon() {
		return von;
	}

	public void setVon(Date von) {
		this.von = von;
	}
	
	public Date getBis() {
		return bis;
	}

	public void setBis(Date bis) {
		this.bis = bis;
	}
	
	public boolean getGenehmigt() {
		return genehmigt;
	}

	public void setGenehmigt(boolean genehmigt) {
		this.genehmigt = genehmigt;
	}


	
	
	
	@Override
	public String toString() {
		return String.format("Benutzer:{id: %d; arbeiterid: %d; ferientage: %f; von: %s; bis: %s, genehmigt: %b }", id, arbeiterid, ferientage, von, bis, genehmigt);
	}
	

}
