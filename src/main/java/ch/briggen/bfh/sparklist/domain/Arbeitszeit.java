package ch.briggen.bfh.sparklist.domain;

import java.sql.Date;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Arbeitszeit {
	
	private int id;
	private int arbeiterid;
	private String eintrag;
	private float arbeitszeit;
	private Date datum;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Arbeitszeit()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Arbeitszeit(int id, int arbeiterid, String eintrag, float arbeitszeit, Date date1)
	{
		this.id = id;
		this.arbeiterid = arbeiterid;
		this.eintrag = eintrag;
		this.arbeitszeit = arbeitszeit;
		this.datum = date1;
	}

	
	public String getEintrag() {
		return eintrag;
	}

	public void setEintrag(String eintrag) {
		this.eintrag = eintrag;
	}

	public float getArbeitszeit() {
		return arbeitszeit;
	}

	public void setArbeitszeit(float arbeitszeit) {
		this.arbeitszeit = arbeitszeit;
	}
	
	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getArbeiterid() {
		return arbeiterid;
	}
	
	public void setArbeiterid(int arbeiterid) {
		this.arbeiterid = arbeiterid;
	}
	
	@Override
	public String toString() {
		return String.format("Arbeitszeit:{id: %d; arbeiterid: %d eintrag: %s; arbeitszeit: %f; datum: %s}", id, arbeiterid, eintrag, arbeitszeit, datum);
	}
	

}
