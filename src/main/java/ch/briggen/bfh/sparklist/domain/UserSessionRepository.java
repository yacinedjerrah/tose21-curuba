package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UserSessionRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ArbeitszeitRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<UserSession> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name from tbl_session");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all UserSessions. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public Collection<UserSession> getUserSessionID()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id from tbl_session");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all UserSessions. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	

	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public static Collection<UserSession> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name from tbl_session where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving UserSessions by Name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public UserSession getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name tbl_session where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_BENUTZER(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving UserSessions by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(UserSession i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_session set name=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating UserSessions " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 */
	public Object loeschen() {
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from tbl_session");
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting UserSessions by id ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
		return null;
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(String i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_session (name) values (?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i);
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting UserSessions " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<UserSession> mapTBL_BENUTZER(ResultSet rs) throws SQLException 
	{
		LinkedList<UserSession> list = new LinkedList<UserSession>();
		while(rs.next())
		{
			UserSession i = new UserSession(rs.getInt("id"),rs.getString("name"));
			list.add(i);
		}
		return list;
	}
	

	


}