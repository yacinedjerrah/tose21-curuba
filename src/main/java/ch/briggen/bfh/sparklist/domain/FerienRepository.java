package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FerienRepository {
	
	private final static Logger log = LoggerFactory.getLogger(ArbeitszeitRepository.class);
	

	/**
	 * Liefert alle Arbeitszeits in der Datenbank
	 * @return Collection aller Arbeitszeits
	 */
	public Collection<Ferien> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select tbl_ferien.id, tbl_ferien.arbeiterid, tbl_ferien.ferientage, tbl_ferien.von, tbl_ferien.bis, tbl_ferien.genehmigt, tbl_benutzer.name, tbl_session.name from tbl_ferien INNER JOIN tbl_benutzer ON tbl_ferien.arbeiterid=tbl_benutzer.id INNER JOIN tbl_session ON tbl_benutzer.name=tbl_session.name");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_FERIEN(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Feriens. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public Collection<Ferien> getAllAdmin()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, ferientage, von, bis, genehmigt from tbl_ferien ORDER BY von DESC");
			ResultSet rs = stmt.executeQuery();
			return mapTBL_FERIEN(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Feriens. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	/**
	 * Liefert alle Arbeitszeits mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public static Collection<Ferien> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, ferientage, von, bis, genehmigt from tbl_ferien where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_FERIEN(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Feriens by Name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Arbeitszeit mit der übergebenen Id
	 * @param id id des Arbeitszeit
	 * @return Arbeitszeit oder NULL
	 */
	public Ferien getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, arbeiterid, ferientage, von, bis, genehmigt from tbl_ferien where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTBL_FERIEN(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Feriens by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Arbeitszeit in der Datenbank. fUPDATE.
	 * @param i
	 */
	public void save(Ferien i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_ferien set arbeiterid=?, ferientage=?, von=?, bis=?, genehmigt=? where id=?");
			stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(2, i.getFerientage());
			stmt.setDate(3, i.getVon());
			stmt.setDate(4, i.getBis());
			stmt.setBoolean(5, i.getGenehmigt());
			stmt.setInt(6, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Feriens " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	public void saveGenehmigen(Ferien i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_ferien set arbeiterid=?, ferientage=?, von=?, bis=?, genehmigt=true where id=?");
			stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(2, i.getFerientage());
			stmt.setDate(3, i.getVon());
			stmt.setDate(4, i.getBis());
			//stmt.setBoolean(5, i.getGenehmigt());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Feriens " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	public void saveGenehmigen2(Ferien i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_ferien set arbeiterid=?, ferientage=?, von=?, bis=?, genehmigt=true where id=?");
			stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(2, i.getFerientage());
			stmt.setDate(3, i.getVon());
			stmt.setDate(4, i.getBis());
			//stmt.setBoolean(5, i.getGenehmigt());
			stmt.setInt(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Feriens " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	public void saveFromUser(Ferien i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update tbl_ferien set arbeiterid=(select tbl_benutzer.id from tbl_session INNER JOIN tbl_benutzer ON tbl_session.name=tbl_benutzer.name), ferientage=?, von=?, bis=?, genehmigt=? where id=?");
			stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(2, i.getFerientage());
			stmt.setDate(3, i.getVon());
			stmt.setDate(4, i.getBis());
			stmt.setInt(5, i.getId());
			stmt.setBoolean(6, i.getGenehmigt());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Feriens " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from tbl_ferien where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Feriens by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Arbeitszeit in der DB. INSERT.
	 * @param i neu zu erstellendes Arbeitszeit
	 * @return Liefert die von der DB generierte id des neuen Arbeitszeits zurück
	 */
	public int insert(Ferien i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_ferien (arbeiterid, ferientage, von, bis, genehmigt) values (?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(2, i.getFerientage());
			stmt.setDate(3, i.getVon());
			stmt.setDate(4, i.getBis());
			stmt.setBoolean(5, i.getGenehmigt());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
	public int insertFromUser(Ferien i) {
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into tbl_ferien (arbeiterid, ferientage, von, bis, genehmigt) values ((select tbl_benutzer.id from tbl_session INNER JOIN tbl_benutzer ON tbl_session.name=tbl_benutzer.name),?,?,?,false)", PreparedStatement.RETURN_GENERATED_KEYS);
			//stmt.setInt(1, i.getArbeiterId());
			stmt.setFloat(1, i.getFerientage());
			stmt.setDate(2, i.getVon());
			stmt.setDate(3, i.getBis());
			//stmt.setBoolean(4, i.getGenehmigt());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			return 0;
		}

	}
	
	public Collection<UserSessionId> userIdFromSession(int userid) {
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select tbl_benutzer.id from tbl_session INNER JOIN tbl_benutzer ON tbl_session.name=tbl_benutzer.name");
			ResultSet rs = stmt.executeQuery();
			return mapUserIdFromSession(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Feriens. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
		
	}
	
	private static Collection<UserSessionId> mapUserIdFromSession(ResultSet rs) throws SQLException {
		LinkedList<UserSessionId> list = new LinkedList<UserSessionId>();
		while(rs.next())
		{
			UserSessionId f = new UserSessionId(rs.getInt("id"));
			list.add(f);
		}
		return list;
	}

	/**
	 * Helper zum konvertieren der Resultsets in Arbeitszeit-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Ferien> mapTBL_FERIEN(ResultSet rs) throws SQLException 
	{
		LinkedList<Ferien> list = new LinkedList<Ferien>();
		while(rs.next())
		{
			Ferien f = new Ferien(rs.getInt("id"),rs.getInt("arbeiterid"),rs.getFloat("ferientage"),rs.getDate("von"),rs.getDate("bis"),rs.getBoolean("genehmigt"));
			list.add(f);
		}
		return list;
	}
	
	/**
	 * Löscht das Arbeitszeit mit der angegebenen Id von der DB
	 * @param id Arbeitszeit ID
	 */
	public boolean login(String username, String password) {
		return false;
		

	}


}