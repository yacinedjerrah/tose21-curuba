package ch.briggen.bfh.sparklist.domain;

import java.sql.Date;




public class Betriebsferien {
	
	private int id;
	private Date von;
	private Date bis;
	private String eintrag;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Betriebsferien()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Betriebsferien(int id, Date von, Date bis, String eintrag)
	{
		this.id = id;
		this.von = von;
		this.bis = bis;
		this.eintrag = eintrag;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Date getVon() {
		return von;
	}

	public void setVon(Date von) {
		this.von = von;
	}
	
	public Date getBis() {
		return bis;
	}

	public void setBis(Date bis) {
		this.bis = bis;
	}
	
	public String getEintrag() {
		return eintrag;
	}

	public void setEintrag(String eintrag) {
		this.eintrag = eintrag;
	}


	
	
	
	@Override
	public String toString() {
		return String.format("Betriebsferien:{id: %d; von: %s; bis: %s, eintrag: %s }", id, von, bis, eintrag);
	}
	

}
