package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class DashboardProfitProKunde {

	private String kunde;
	private int profit;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public DashboardProfitProKunde()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public DashboardProfitProKunde(String kunde, int profit)
	{
		this.kunde = kunde;
		this.profit = profit;
	}

	public String getKunde() {
		return kunde;
	}

	public void setKunde(String kunde) {
		this.kunde = kunde;
	}
	
	public int getProfit() {
		return profit;
	}

	public void setProfit(int profit) {
		this.profit = profit;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return String.format("{profit: %.2d; kunde: %s }", profit, kunde);
	}
	

}
