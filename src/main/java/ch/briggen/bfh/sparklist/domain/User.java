package ch.briggen.bfh.sparklist.domain;





public class User {
	
	private int id;
	private String name;
	private String password;
	private int pensum;
	private float ferien;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public User()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public User(int id, String name, String password, int pensum, float ferien)
	{
		this.id = id;
		this.name = name;
		this.password = password;
		this.pensum = pensum;
		this.ferien = ferien;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@Override
	public String toString() {
		return String.format("Benutzer:{id: %d; name: %s; pw: %s;}", id, name, password);
	}

	public int getPensum() {
		return pensum;
	}

	public void setPensum(int pensum) {
		this.pensum = pensum;
	}

	public float getFerien() {
		return ferien;
	}

	public void setFerien(float ferien) {
		this.ferien = ferien;
	}
	

}
